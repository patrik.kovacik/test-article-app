FROM python:3.10-alpine
WORKDIR /basic-flask-api
ADD . /basic-flask-api
# install psycopg2 dependencies
RUN apk update
RUN apk add postgresql-dev gcc python3-dev musl-dev
# install requirements
RUN pip install -r requirements.txt
CMD ["python","app.py"]
