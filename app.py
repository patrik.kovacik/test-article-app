from flask import Flask, redirect, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:password@localhost/flaskarticles'
db = SQLAlchemy(app)

class Articles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(500), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Article %r>' % self.id

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        article_content = request.form['content']
        new_article = Articles(content=article_content)

        try:
            db.session.add(new_article)
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue adding your article'

    else:
        articles = Articles.query.order_by(Articles.date_created).all()
        
        return render_template('index.html', articles=articles)

@app.route('/delete/<int:id>')
def delete(id):
    article_to_delete = Articles.query.get_or_404(id)

    try:
        db.session.delete(article_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that article'

@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    article = Articles.query.get_or_404(id)
    if request.method == 'POST':
        article.content = request.form['content']

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was and issue updating your task'

    else:
        return render_template('update.html', article=article)


if __name__ == "__main__":
    app.run(debug=True)